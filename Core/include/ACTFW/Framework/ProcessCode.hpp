//
//  ProcessCode.h
//  ACTFW
//
//  Created by Andreas Salzburger on 11/05/16.
//
//

#ifndef ACTFW_FRAMEWORK_PROCESSCODE_H
#define ACTFW_FRAMEWORK_PROCESSCODE_H 1

namespace FW {
enum class ProcessCode { SUCCESS, ABORT, END };
}

#endif  // ACTFW_FRAMEWORK_PROCESSCODE_H
